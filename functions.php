<?php

/*
* Add your own functions here. You can also copy some of the theme functions into this file. 
* Wordpress will use those functions instead of the original functions then.
*/

add_filter('avf_logo','av_change_logo');
function av_change_logo($logo)
{
	$lang = pll_current_language('locale');

	switch ($lang) {
    case 'en_US':
        $logo = "/wp-content/themes/vertellen/images/logo-en.png";
        break;
    case 'nl_NL':
        $logo = "/wp-content/themes/vertellen/images/logo-nl.png";
        break;
    case 'de_DE':
        $logo = "/wp-content/themes/vertellen/images/logo-de.png";
        break;
	}

	return $logo;
}